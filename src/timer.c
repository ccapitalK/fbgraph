#include"timer.h"
#include<sys/time.h>
#include<stdlib.h>

Timer new_timer(){
    return malloc(sizeof(struct _timer));
}

void update_timer(Timer tm){
    gettimeofday(&(tm->tv),NULL);
    tm->secStart=tm->tv.tv_sec;
    tm->usecStart=tm->tv.tv_usec;
}

long int get_microseconds(Timer tm){
    gettimeofday(&(tm->tv),NULL);
    return (tm->tv.tv_sec-tm->secStart)*1000000+(tm->tv.tv_usec-tm->usecStart);
}

void destroy_timer(Timer tm){
    free(tm);
}
