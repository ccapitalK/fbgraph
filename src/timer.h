#ifndef FBGRAPH_TIMER_H
#define FBGRAPH_TIMER_H
#ifdef __cplusplus
extern "C" {
#endif

#define START_DEBUG_TIMER() Timer tm = newTimer(); \
    updateTimer(tm);
#define END_DEBUG_TIMER() long int timeElapsed = getMicroseconds(tm); \
    fprintf(stderr, "Time taken for %s(): %ld.%06ld sec\n", __func__, timeElapsed/1000000, timeElapsed%1000000); \
    destroyTimer(tm);

#include<sys/time.h>
typedef struct _timer * Timer;

struct _timer{
    struct timeval tv;
    long int secStart;
    long int usecStart;
};

Timer new_timer();
void update_timer(Timer tm);
long int get_microseconds(Timer tm);
void destroy_timer(Timer tm);
#ifdef __cplusplus
}
#endif
#endif
