#include"graph.h"
#include"timer.h"
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<unistd.h>

#include<assert.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<linux/fb.h>
#include<sys/ioctl.h>
#include<sys/mman.h>
#include<ctype.h>
#include<stdint.h>

Screen init_fb(void){

    if(getenv("DISPLAY")!=NULL){
        ERROR("Currently, this program does not support X11");
        goto FLenv;
    }

    Screen retScreen = malloc(sizeof(struct _screen)); 
    if( retScreen == NULL){ 
        ERROR("Couldn't Allocate space for screen");
        goto FLalloc;
    }

    if( (retScreen->fd = open("/dev/fb0", O_RDWR)) == -1 ){
        ERROR("Couldn't open /dev/fb0");
        goto FLopen;
    } 

    if(ioctl(retScreen->fd, FBIOGET_FSCREENINFO, &(retScreen->finfo)) == -1){
        ERROR("Couldn't call FBIOGET_FSCREENINFO ioctl()");
        goto FLgetInfo;
    }

    if(ioctl(retScreen->fd, FBIOGET_VSCREENINFO, &(retScreen->vinfo)) == -1){
        ERROR("Couldn't call FBIOGET_VSCREENINFO ioctl()");
        goto FLgetInfo;
    }

    if(retScreen->vinfo.bits_per_pixel!=32){
        ERROR("This library only currently supports 32bit framebuffers");
        goto FLgetInfo;
    }
    {
        retScreen->xres = retScreen->vinfo.xres_virtual;
        retScreen->yres = retScreen->vinfo.yres_virtual;
        retScreen->total_size = retScreen->xres * retScreen->yres * retScreen->vinfo.bits_per_pixel/8;
    }

    //printf("%dx%d, %d bpp\n", retScreen->xres, retScreen->yres, retScreen->vinfo.bits_per_pixel);
    //printf("Total Size: %lu\n", retScreen->total_size);

    retScreen->data = (unsigned char*)mmap(NULL, retScreen->total_size, PROT_READ | PROT_WRITE, MAP_SHARED, retScreen->fd, 0);
    if(retScreen->data==MAP_FAILED){
        ERROR("Could not mmap() /dev/fb0");
        goto FLgetInfo;
    }

    return retScreen;

    
    //list of things to clear up if an error is encountered
    FLgetInfo:              
        close(retScreen->fd);
    FLopen:                 
        free(retScreen);
    FLenv:
    FLalloc:                
        return NULL;
}

Screen init_screen(uint32_t xres, uint32_t yres){
    Screen retScreen = malloc(sizeof(struct _screen)); 
    if( retScreen == NULL){ 
        ERROR("Couldn't Allocate space for screen");
        return NULL;
    }
    retScreen->total_size=xres*yres*4;
    retScreen->data=malloc(retScreen->total_size);
    if(retScreen->data==NULL){
        ERROR("Couldn't Allocate space for screen->data");
        free(retScreen);
        return NULL;
    }
    retScreen->xres=xres;
    retScreen->yres=yres;
    return retScreen;
}

void blit_screen(Screen source, Screen dest, uint16_t x, uint16_t y){
    //uint32_t x2=x+source->xres;
    //uint32_t y2=y+source->yres;
    for(unsigned int j = 0; j < MIN(source->yres,dest->yres-y); ++j){
        uint8_t * sourcePos = &(source->data[4*j*source->xres]);
        uint8_t * destPos   = &(dest->data[4*((y+j)*dest->xres+x)]);
        memcpy(destPos,sourcePos,4*MIN(source->xres,dest->xres-x));
    }
}

void destroy_screen(Screen screen){
    if(screen->fd!=-1)
        close(screen->fd);
    free(screen);
}

void draw_rect_filled(Screen screen, uint32_t x0, uint32_t y0, uint32_t x1, uint32_t y1, uint8_t r, uint8_t g, uint8_t b){
    for(uint32_t i = y0; i < y1; ++i){
        for(uint32_t j = x0; j < x1; ++j){
            PUT_PIXEL(screen,j,i,r,g,b);
        }
    }
}

void draw_line(Screen screen, uint32_t x0, uint32_t y0, uint32_t x1, uint32_t y1, uint8_t r, uint8_t g, uint8_t b){
    char stepDir;
    int32_t dx = x1-x0;
    int32_t dy = y1-y0;
    uint32_t x;
    uint32_t y;
    int32_t xstep=dx>0?1:-1;
    int32_t ystep=dy>0?1:-1;

    if(abs(dx)>abs(dy)){
        stepDir='x';
    }else{
        stepDir='y';
    }

    if(dx==0){
        if(dy==0){
            PUT_PIXEL(screen,x0,y0,r,g,b);
        }else{
            x=x0;
            for(y = y0; y != y1+ystep; y+=ystep){
                PUT_PIXEL(screen,x,y,r,g,b);
            }
        }
    }else if(dy==0){
        y=y0;
        for(x = x0; x != x1+xstep; x+=xstep){
            PUT_PIXEL(screen,x,y,r,g,b);
        }
    }else{
        if(stepDir=='x'){
            for(x = x0; x != x1+xstep; x+=xstep){
                y=y0+xstep*(dy*abs(x-x0))/dx;
                PUT_PIXEL(screen,x,y,r,g,b);
            }
        }else{
            for(y = y0; y != y1+ystep; y+=ystep){
                x=x0+ystep*(dx*abs(y-y0))/dy;
                PUT_PIXEL(screen,x,y,r,g,b);
            }
        }
    }
}

void draw_circle(Screen screen, uint32_t x, uint32_t y, uint32_t rad, uint32_t thick, uint8_t r, uint8_t g, uint8_t b){
    uint32_t mx=rad;
    uint32_t my=0;
    while(mx>my){
        while(mx*mx+my*my>rad*rad)--mx;
        uint32_t tx = mx+thick/2;
        uint32_t bx = tx-thick;
        for(uint32_t cx = tx; cx>=bx; --cx){
            PUT_PIXEL(screen,x+cx,y+my,r,g,b);
            PUT_PIXEL(screen,x-cx,y+my,r,g,b);
            PUT_PIXEL(screen,x+cx,y-my,r,g,b);
            PUT_PIXEL(screen,x-cx,y-my,r,g,b);
            PUT_PIXEL(screen,x+my,y+cx,r,g,b);
            PUT_PIXEL(screen,x+my,y-cx,r,g,b);
            PUT_PIXEL(screen,x-my,y+cx,r,g,b);
            PUT_PIXEL(screen,x-my,y-cx,r,g,b);
        }
        ++my;
    }
}
void draw_circle_filled(Screen screen, uint32_t x, uint32_t y, uint32_t rad, uint8_t r, uint8_t g, uint8_t b){
    uint32_t mx=rad;
    uint32_t my=0;
    if(rad){
        PUT_PIXEL(screen,x,y,r,g,b);
    }
    while(mx>my){
        while(mx*mx+my*my>rad*rad)--mx;
        for(uint32_t cx = mx; cx>0; --cx){
            PUT_PIXEL(screen,x+cx,y+my,r,g,b);
            PUT_PIXEL(screen,x-cx,y+my,r,g,b);
            PUT_PIXEL(screen,x+cx,y-my,r,g,b);
            PUT_PIXEL(screen,x-cx,y-my,r,g,b);
            PUT_PIXEL(screen,x+my,y+cx,r,g,b);
            PUT_PIXEL(screen,x+my,y-cx,r,g,b);
            PUT_PIXEL(screen,x-my,y+cx,r,g,b);
            PUT_PIXEL(screen,x-my,y-cx,r,g,b);
        }
        ++my;
    }
}

void draw_rect(Screen screen, uint32_t x0, uint32_t y0, uint32_t x1, uint32_t y1, uint8_t r, uint8_t g, uint8_t b){
    draw_line(screen,x0  ,y0  ,x1-1,y0  ,r,g,b);
    draw_line(screen,x0  ,y0  ,x0  ,y1-1,r,g,b);
    draw_line(screen,x1-1,y0  ,x1-1,y1-1,r,g,b);
    draw_line(screen,x0  ,y1-1,x1-1,y1-1,r,g,b);
}

void draw_rect_thick(Screen screen, uint32_t x0, uint32_t y0, uint32_t x1, uint32_t y1, uint16_t thick, uint8_t r, uint8_t g, uint8_t b){
    draw_rect_filled(screen,x0,y0,x1,y0+thick,r,g,b);
    draw_rect_filled(screen,x0,y0,x0+thick,y1,r,g,b);
    draw_rect_filled(screen,x1-thick-1,y0,x1,y1,r,g,b);
    draw_rect_filled(screen,x0,y1-thick-1,x1,y1,r,g,b);
}

void draw_triangle(Screen screen, int32_t x[3], int32_t y[3], uint8_t r, uint8_t g, uint8_t b){
    int32_t yStart = MIN(y[0],MIN(y[1],y[2]));
    int32_t yEnd =   MAX(y[0],MAX(y[1],y[2]));
    //printf("%d %d\n", yStart, yEnd);
    for(int32_t yi = yStart; yi <= yEnd; ++yi){
        int32_t xStart = MAX(x[0],MAX(x[1],x[2])); 
        int32_t xEnd   = MIN(x[0],MIN(x[1],x[2]));
        for(int i = 0; i < 3; ++i){
            int32_t small = (y[i]<=y[(i+1)%3]?i:(i+1)%3),
                    large = (y[i]>=y[(i+1)%3]?i:(i+1)%3);
            if(CLAMP(y[small],yi,y[large])==yi){
                if(y[small]==y[large]){
                    xStart = MIN(MIN(x[small],x[large]),xStart);
                    xEnd   = MAX(MAX(x[small],x[large]),xEnd);
                }else{
                    int32_t xPos = x[small]+((yi-y[small])*(x[large]-x[small]))/(y[large]-y[small]);
                    //printf("%d: [%d %d] %d + (%d*%d)/%d=%d\n", yi, small, large, x[small], (yi-y[small]), (x[large]-x[small]), y[large]-y[small], xPos);
                    xStart=MIN(xStart,xPos);
                    xEnd=MAX(xEnd,xPos);
                }
            }
        }
        //printf("%d: %d-%d\n", yi, xStart, xEnd);
        for(int32_t xi = xStart; xi <= xEnd; ++xi){
            PUT_PIXEL(screen,xi,yi,r,g,b);
        }
    }
}

void draw_triangle_p(Screen screen, int32_t x0, int32_t y0, int32_t x1, int32_t y1, int32_t x2, int32_t y2, uint8_t r, uint8_t g, uint8_t b){
    int32_t x[] = {x0, x1, x2};
    int32_t y[] = {y0, y1, y2};
    draw_triangle(screen,x,y,r,g,b);
}
