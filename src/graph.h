#ifndef FBGRAPH_GRAPH_H
#define FBGRAPH_GRAPH_H
#ifdef __cplusplus
extern "C" {
#endif
#include<linux/fb.h>
#include<stdlib.h>
#include<ctype.h>
#include<stdint.h>
#define MIN(a,b) (a < b ? a : b)
#define MAX(a,b) (a > b ? a : b)
#define CLAMP(min,val,max) (MIN(max,MAX(min,val)))
#define ERROR(x) fprintf(stderr, x "\n")
#define PARAMETER_ERROR() fprintf(stderr, "Malformed Parameters in call to %s()\n", __func__); exit(EXIT_FAILURE);

#define PUT_PIXEL(screen, x, y, r, g, b) { \
        uint32_t offset = 4*((y)*(screen)->xres+(x)); \
        if(offset < (screen)->total_size){ \
            screen->data[offset+0]=b; \
            screen->data[offset+1]=g; \
            screen->data[offset+2]=r; \
        } \
    }

typedef struct _screen * Screen;

struct _screen{
    int fd;
    struct fb_var_screeninfo vinfo;
    struct fb_fix_screeninfo finfo;
    uint32_t xres;
    uint32_t yres;
    size_t total_size;
    uint8_t * data;
};

Screen init_fb(void);
Screen init_screen(uint32_t xres, uint32_t yres);
void blit_screen(Screen source, Screen dest, uint16_t x, uint16_t y);
void destroy_screen(Screen screen);

void draw_rect(Screen screen, uint32_t x0, uint32_t y0, uint32_t x1, uint32_t y1, uint8_t r, uint8_t g, uint8_t b);
void draw_rect_filled(Screen screen, uint32_t x0, uint32_t y0, uint32_t x1, uint32_t y1, uint8_t r, uint8_t g, uint8_t b);
void draw_rect_thick(Screen screen, uint32_t x0, uint32_t y0, uint32_t x1, uint32_t y1, uint16_t thick, uint8_t r, uint8_t g, uint8_t b);
void draw_line(Screen screen, uint32_t x0, uint32_t y0, uint32_t x1, uint32_t y1, uint8_t r, uint8_t g, uint8_t b); 
void draw_circle(Screen screen, uint32_t x, uint32_t y, uint32_t rad, uint32_t thick, uint8_t r, uint8_t g, uint8_t b);
void draw_circle_filled(Screen screen, uint32_t x, uint32_t y, uint32_t rad, uint8_t r, uint8_t g, uint8_t b);
void draw_triangle(Screen screen, int32_t x[3], int32_t y[3], uint8_t r, uint8_t g, uint8_t b);
void draw_triangle_p(Screen screen, int32_t x0, int32_t y0, int32_t x1, int32_t y1, int32_t x2, int32_t y2, uint8_t r, uint8_t g, uint8_t b);

#ifdef __cplusplus
}
#endif
#endif
