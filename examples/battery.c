#include<graph.h>
#include<timer.h>
#include<stdio.h>
#include<stdint.h>
#include<time.h>
#include<unistd.h>
#include<string.h>

void drawBattery(Screen renderTarg, uint16_t x, uint16_t y, int batteryPercentage, int isCharging);
int getBatteryPercentage(void);
int getBatteryChargingStatus(void);

int main(){
    Screen masterScreen = init_fb();

    while(1){
        drawBattery(masterScreen, 1300, 1000, getBatteryPercentage(), getBatteryChargingStatus());
        sleep(1);
    }

    destroy_screen(masterScreen);
    return EXIT_SUCCESS;
}

int getBatteryChargingStatus(void){
    char progBuf[256];
    FILE * acpi = popen("acpi","r");
    if(acpi==NULL){
        ERROR("Could not get battery status from exec(\"acpi\")");
        exit(EXIT_FAILURE);
    }
    fscanf(acpi, "%[^\n]", progBuf);
    int retValue=0;
    //if(strstr(progBuf, "Charging")!=NULL || strstr(progBuf, "Full")!= NULL){
    if(strstr(progBuf, "Charging")!=NULL){
        retValue=1;
    }
    pclose(acpi);
    return retValue;
}

int getBatteryPercentage(void){
    FILE * acpi = popen("acpi","r");
    if(acpi==NULL){
        ERROR("Could not get battery percentage from exec(\"acpi\")");
        exit(EXIT_FAILURE);
    }
    int retValue=-1;
    fscanf(acpi, "%*[^0-9]%*[0-9]%*[^0-9]%d", &retValue);
    pclose(acpi);
    if(retValue==-1){
        ERROR("Could not get battery percentage from exec(\"acpi\")");
        exit(EXIT_FAILURE);
    }
    return retValue;
}

void drawBattery(Screen renderTarg, uint16_t x, uint16_t y, int batteryPercentage, int isCharging){
    //width 128
    //height 64

    //25x44
    int32_t pX[6] = { x+74, x+58, x+70, x+66, x+82, x+70 };
    int32_t pY[6] = { y+18, y+34, y+34, y+46, y+30, y+30 };
    draw_rect_filled(renderTarg,x,y,x+128,y+64,0,0,0);
    draw_rect_thick(renderTarg, x+15, y+2, x+128, y+62, 5, 255, 255, 255);
    draw_rect_thick(renderTarg, x+2, y+16, x+19, y+48, 5, 255, 255, 255);
    if(batteryPercentage<=20){
        draw_rect_filled(renderTarg,x+121-batteryPercentage,y+8,x+121,y+55,255,  0,  0);
    }else if(batteryPercentage<=50){
        draw_rect_filled(renderTarg,x+121-batteryPercentage,y+8,x+121,y+55,255,255,  0);
    }else if(batteryPercentage<100){
        draw_rect_filled(renderTarg,x+121-batteryPercentage,y+8,x+121,y+55,255,255,255);
    }else{
        draw_rect_filled(renderTarg,x+121-batteryPercentage,y+8,x+121,y+55,  0,255,  0);
    }

    if(isCharging){
        draw_triangle_p(renderTarg, pX[0], pY[0], pX[1], pY[1], pX[5], pY[5], 255, 0, 0);
        draw_triangle_p(renderTarg, pX[1], pY[1], pX[2], pY[2], pX[5], pY[5], 255, 0, 0);
        draw_triangle_p(renderTarg, pX[3], pY[3], pX[4], pY[4], pX[2], pY[2], 255, 0, 0);
        draw_triangle_p(renderTarg, pX[4], pY[4], pX[5], pY[5], pX[2], pY[2], 255, 0, 0);
        draw_line(     renderTarg, pX[0], pY[0], pX[1], pY[1], 200, 0, 0);
        draw_line(     renderTarg, pX[1], pY[1], pX[2], pY[2], 200, 0, 0);
        draw_line(     renderTarg, pX[2], pY[2], pX[3], pY[3], 200, 0, 0);
        draw_line(     renderTarg, pX[3], pY[3], pX[4], pY[4], 200, 0, 0);
        draw_line(     renderTarg, pX[4], pY[4], pX[5], pY[5], 200, 0, 0);
        draw_line(     renderTarg, pX[5], pY[5], pX[0], pY[0], 200, 0, 0);
    }
}
