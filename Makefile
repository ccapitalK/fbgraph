CC=gcc
CFLAGS=-Wall -Wextra -O2 -lm -Isrc
LIB_FILES=src/graph.c src/timer.c
DEBUG_FLAGS=
VERSION=0.0.2
EXAMPLE_BINS=bin/battery bin/clock bin/blank

.PHONY: all snapshot examples clean

.DEFAULT_GOAL:=all

all: lib/libfbgraph.so examples

snapshot: snapshot/libfbgraph-${VERSION}.tar.gz

examples: ${EXAMPLE_BINS}

bin/%: examples/%.c lib/libfbgraph.so
	${CC} ${CFLAGS} -lfbgraph -Isrc -Llib/ -Wl,-R -Wl,lib/ $< -o $@

lib/libfbgraph.so: ${LIB_FILES}
	${CC} ${CFLAGS} ${LINK_FLAGS} ${LIB_FILES} -fPIC -shared -o lib/libfbgraph.so

snapshot/libfbgraph-${VERSION}.tar.gz:
	rm snapshot/libfbgraph-*.tar.gz || true
	git archive master | gzip > snapshot/libfbgraph-${VERSION}.tar.gz

clean:
	rm ${EXAMPLE_BINS} lib/libfbgraph.so
